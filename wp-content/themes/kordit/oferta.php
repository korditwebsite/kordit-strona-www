<?php
/**
* Template Name: Oferta
*/
?>
<?php get_header(); ?>
<main id="oferta">
	<canvas id="linie" width="1550" height="400"></canvas>
	<section id="service" class="panel" data-section-name="service">
		<div class="content-tag">
			<?php
			$nazwaKategori = get_field("nazwa_kategorii");
			$args = array(
				'post_type'   => "oferta",
				'post_status' => 'publish',
				'posts_per_page' => '99',
				'order' => 'ASC',
				'orderby' => 'name'
			);

			$testimonials = new WP_Query( $args );
			if( $testimonials->have_posts() ) :
				?>
				<div class="inner-content wow fadeInLeft">
					<div class="row">
						<div class="col-xl-8 offset-xl-2">
							<div class="col-xl-6">
								<ul class="nav">
									<?php $menu = 0;
									while( $testimonials->have_posts() ) :
										$testimonials->the_post();
										?>
										<li>
											<a data-toggle="tab" href="#menu<?php echo $menu; ?>"><?php the_title(); ?> </a>
										</li>
										<?php
										$menu = $menu + 1;
									endwhile;
									wp_reset_postdata();
									?>

								</ul>
							</div>
						</div>
						<div class="col-xl-12">
							<div class="col-xl-6 offset-xl-6">
								<div class="tab-content">
									<?php $menucontent = 0;
									while( $testimonials->have_posts() ) :
										$testimonials->the_post();
										?>
										<div id="menu<?php echo $menucontent; ?>" class="tab-pane fade">
											<h3><?php the_title(); ?> </h3>
											<div class="opis">
												<?php the_field('krotki_opis'); ?>
											</div>
											<a href="<?php echo get_permalink( $post->ID ); ?>">Czytaj więcej</a>
										</div>
										<?php
										$menucontent = $menucontent + 1;
									endwhile;
									wp_reset_postdata();
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php
			else :
				esc_html_e( 'Ta kategoria jest pusta, uzupełnij ją!', 'text-domain' );
			endif;
			?>
			<div class="fly-letter">
				<span>oferta</span>
			</div>
		</div>
	</section>
</main>
<?php get_footer(); ?>