<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="google-site-verification" content="aViWC_fdJ8s6xWort5qaWiQc5Hs0bjwLllFqna68Mjo" />
	<?php wp_head(); ?>
	<title><?php wp_title('the_title_attribute();'); ?></title>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116345191-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-116345191-1');
	</script>
	
	<script>
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};
			if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
			n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t,s)}(window, document,'script',
				'https://connect.facebook.net/en_US/fbevents.js');
			fbq('init', '1098169100361389');
			fbq('track', 'PageView');
		</script>
		<noscript><img alt="strony internetowe Lublin" height="1" width="1" style="display:none"
			src="https://www.facebook.com/tr?id=1098169100361389&ev=PageView&noscript=1"
			/></noscript>
			<!-- End Facebook Pixel Code -->

		</head>
		<body <?php body_class( 'class-name' ); ?>>
			<div id="particles-js"></div>
			<nav id="mainmenu">
				<?php
				wp_nav_menu([
					'menu'            => 'top',
					'theme_location'  => 'top',
					'container'       => 'div',
					'container_id'    => 'bs4navbar',
					'container_class' => 'collapse navbar-collapse show',
					'menu_class'      => 'navbar-nav',
					'depth'           => 2,
					'fallback_cb'     => 'bs4navwalker::fallback',
					'walker'          => new bs4navwalker(),
				]);
				?>
			</nav>
			<!-- Main navigation -->
			<header>
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-6 col-6">
							<div class="navbar-brand">
								<a href="/">
									<?php $custom_logo_id = get_theme_mod( 'custom_logo' );
									$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
									if ( has_custom_logo() ) {
										echo '<img alt="logotyp" class="img-fluid" src="'. esc_url( $logo[0] ) .'">';
									} else {
										echo '<h1>'. get_bloginfo( 'name' ) .'</h1>';
									} ?>
								</a>
							</div>
						</div>
						<div class="col-xl-6 col-6 flex-right">
							<div id="menu-button">
								<span></span>
								<span></span>
								<span></span>
								<span></span>
								<span></span>
								<span></span>
							</div>
						</div>
					</div>
				</div>
			</header>
			



