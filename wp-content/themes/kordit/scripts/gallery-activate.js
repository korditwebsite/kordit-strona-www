window.addEventListener('DOMContentLoaded',function(){
	new SmartPhoto(".js-smartPhoto");
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var FilterGallery = function () {
	function FilterGallery() {
		_classCallCheck(this, FilterGallery);

		this.$filtermenuList = $('.filtermenu li');
		this.$container = $('.contain');

		this.updateMenu('all');
		this.$filtermenuList.on('click', $.proxy(this.onClickFilterMenu, this));
	}

	FilterGallery.prototype.onClickFilterMenu = function onClickFilterMenu(event) {
		var $target = $(event.target);
		var targetFilter = $target.data('filter');

		this.updateMenu(targetFilter);
		this.updateGallery(targetFilter);
	};

	FilterGallery.prototype.updateMenu = function updateMenu(targetFilter) {
		this.$filtermenuList.removeClass('active');
		this.$filtermenuList.each(function (index, element) {
			var targetData = $(element).data('filter');

			if (targetData === targetFilter) {
				$(element).addClass('active');
			}
		});
	};
	
	FilterGallery.prototype.updateGallery = function updateGallery(targetFilter) {
		var _this = this;

		if (targetFilter === 'all') {
			this.$container.fadeOut(300, function () {
				$('.post').show();
				_this.$container.fadeIn();
			});
		} else {
			this.$container.find('.post').each(function (index, element) {
				_this.$container.fadeOut(300, function () {
					if ($(element).hasClass(targetFilter)) {
						$(element).show();
					} else {
						$(element).hide();
					}
					_this.$container.fadeIn();
				});
			});
		}
	};

	return FilterGallery;
}();

var fliterGallery = new FilterGallery();
/**
 * @author mrdoob / http://mrdoob.com/
 */

 