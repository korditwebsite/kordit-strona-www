<?php
/**
* Template Name: Wszystkie realizacje
*/
?>
<?php get_header(); ?>
<main id="realizacje">
	<canvas id="linie" width="1550" height="400"></canvas>
	<section id="portfolio" class="panel" data-section-name="portfolio">
		<div class="inner-content">
			<h3 class="title">Portfolio</h3>
			<div class="container">
				<article class="row">
					<div class="col-xl-12">
						<ul class="filtermenu nav wow fadeInRight">
							<?php
							$terms = get_terms('rodzaj');
							$count = count($terms);
							echo '<li data-filter="all">Wszystkie</li>';
							if ( $count > 0 ){
								foreach ( $terms as $term ) {
									$termname = strtolower($term->name);
									$termname = str_replace(' ', '-', $termname);
									echo '<li data-filter="'.$termname.'">'.$term->name.'</li>';
								}
							}
							?>
						</ul>
						<div class="tab-content wow fadeInLeft">
							<div class="contain row">
								<?php
								$args = array(
									'post_type'   => "realizacja",
									'post_status' => 'publish',
									'order' => 'ASC'
								);
								$testimonials = new WP_Query( $args );
								if( $testimonials->have_posts() ) :
									$args = array( 'post_type' => 'realizacja', 'posts_per_page' => -1 );
									$loop = new WP_Query( $args );
									while ( $loop->have_posts() ) : $loop->the_post();
										$terms = get_the_terms( $post->ID, 'rodzaj' );
										if ( $terms && ! is_wp_error( $terms ) ) :
											$links = array();
											foreach ( $terms as $term ) {
												$links[] = $term->name;
											}
											$tax_links = join( " ", str_replace(' ', '-', $links));
											$tax = strtolower($tax_links);
										endif;

										echo '<div class="col-xl-4 col-md-6 col-sm-6 col-xs-12 post '. $tax .'">';
										echo '<a href="'. get_permalink() .'">';
										echo '<div class="box" style="background-image: url('. wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ) .');">';
										echo '<div class="box-info"><p>';
										the_title();
										echo '</p></div>';
										echo '</div>';
										echo '</a>';
										echo '</div>';
									endwhile; ?>
									<?php
								else :
									esc_html_e( 'Ta kategoria jest pusta, uzupełnij ją!', 'text-domain' );
								endif;
								?>
							</div>
						</div>
					</div>
				</article>
			</div>
		</div>
		<div class="fly-letter">
			<span>portfolio</span>
		</div>
		<aside>
		</aside>
	</section>
</main>
<?php get_footer(); ?>