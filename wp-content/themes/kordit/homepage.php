<?php
/**
* Template Name: Strona główna
*/
?>
<?php get_header(); ?>
<main id="homepage" data-spy="scroll" data-target="#list-spy" data-offset="0">
	<canvas id="linie" width="1550" height="400"></canvas>
	<?php if( have_rows('hero_image') ): while( have_rows('hero_image') ): the_row(); ?>
		<section id="home" class="panel" data-section-name="home">
			<div class="content-tag"> 
				<div class="inner-content">
					<div class="logo-hero animated fadeInUp">

						<?php echo wp_get_attachment_image( get_sub_field('logotyp'), "big", "", array( "class" => "img-responsive", "alt" => "strony internetowe Lublin" ) );  ?>
					</div>
					<h1 class="animated fadeInUp"><?php the_sub_field('podpis'); ?></h1>
				</div>
				<div class="social-bar">
					<ul>
						<li>
							<a  aria-label="Facebook" target="_blank" rel="noopener" href="https://www.facebook.com/korditkck/">
								<i class="fab fa-facebook-f"></i>
							</a>
						</li>
						<li>
							<a  aria-label="Google Plus" target="_blank" rel="noopener" href="https://plus.google.com/u/0/b/108659218017341693225/108659218017341693225#">
								<i class="fab fa-google-plus-g"></i>
							</a>
						</li>
						<li>
							<a  aria-label="Instagram" target="_blank" rel="noopener" href="https://www.instagram.com/korditkck/">
								<i class="fab fa-instagram"></i>
							</a>
						</li>
					</ul>
				</div>
			</div>
			<aside> 
				<div class="menu-nawigacja-home-container">
					<ul id="list-spy" class="pagination">
						<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7 nav-item">
							<a href="#home" class="nav-link">
								<span>Home</span>
							</a>
						</li>
						<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-8 nav-item">
							<a href="#about" class="nav-link">
								<span>O nas</span>
							</a>
						</li>
						<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-9 nav-item">
							<a href="#service" class="nav-link">
								<span>Oferta</span>
							</a>
						</li>
						<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10 nav-item">
							<a href="#portfolio" class="nav-link ">
								<span>Portfolio</span>
							</a>
						</li>
						<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11 nav-item">
							<a href="#contact" class="nav-link active">
								<span>Kontakt</span>
							</a>
						</li>
					</ul>
				</div>
			</aside>
		</section>
	<?php endwhile; endif; ?>
	<?php if( have_rows('o_nas') ): while( have_rows('o_nas') ): the_row(); ?>
		<section id="about" class="panel" data-section-name="about">
			<div class="content-tag">
				<div class="row">
					<div class="col-xl-7">
						<div class="item">
							<h2 class="wow fadeInLeft"><?php the_sub_field("tytul"); ?></h2>
							<p class="wow fadeInLeft">
								<?php the_sub_field("podpis"); ?>
							</p>
						</div>
					</div>
					<div class="col-xl-5">
						<div id="carouselExampleControls" class="carousel slide carousel-fade" data-ride="carousel">
							<div class="carousel-inner">
								<?php
								if( have_rows('osoby') ): $activepeople = true; while ( have_rows('osoby') ) : the_row();
									if ($activepeople === true) {
										$echoactive = "active";
									} else {
										$echoactive = "";
									}
									?>
									<div class="carousel-item <?php echo $echoactive; ?>">
										<div class="informacje">
											<h3><?php the_sub_field("imie_i_nazwisko"); ?></h3>
											<span class="trescomnie"><?php the_sub_field("opis"); ?></span>
										</div>
										<div class="ratio">
											<img class="ratio__content lazyload" alt="strony internetowe Lublin" data-src="<?php echo wp_get_attachment_image_url( get_sub_field('grafika'), "big");  ?>">
										</div>

										<?php //echo wp_get_attachment_image( get_sub_field('grafika'), "big", "", array( "class" => "img-responsive", "alt" => "członek zespołu" ) );  ?>
										
									</div>
									<?php $activepeople = false; endwhile; else : endif; ?>
								</div>
								<div class="carousel-navi">
									<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
										<img class="ratio__content lazyload" alt="strony internetowe Lublin" data-src="/wp-content/uploads/2019/01/arow.png"> 
									</a>
									<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
										<img class="ratio__content lazyload" alt="strony internetowe Lublin" data-src="/wp-content/uploads/2019/01/arow-1-e1546792040483.png"> 
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		<?php endwhile; endif; ?>
		<section id="service" class="panel" data-section-name="service">
			<div class="content-tag">
				<?php
				$nazwaKategori = get_field("nazwa_kategorii");
				$args = array(
					'post_type'   => "oferta",
					'post_status' => 'publish',
					'posts_per_page' => '20',
					'order' => 'ASC',
					'orderby' => 'name'
				);

				$testimonials = new WP_Query( $args );
				if( $testimonials->have_posts() ) :
					?>
					<div class="inner-content wow fadeInLeft">
						<h3 class="title offset-xl-2">Oferta</h3>
						<div class="row">
							<div class="col-xl-8 offset-xl-2">
								<div class="col-xl-6">
									<ul class="nav">
										<?php $menu = 0;
										while( $testimonials->have_posts() ) :
											$testimonials->the_post();
											?>
											<li>
												<a data-toggle="tab" href="#menu<?php echo $menu; ?>"><?php the_title(); ?> </a>
											</li>
											<?php
											$menu = $menu + 1;
										endwhile;
										wp_reset_postdata();
										?>
									</ul>
								</div>
							</div>
							<div class="col-xl-12">
								<div class="col-xl-6 offset-xl-6">
									<div class="tab-content">
										<?php $menucontent = 0;
										while( $testimonials->have_posts() ) :
											$testimonials->the_post();
											?>
											<div id="menu<?php echo $menucontent; ?>" class="tab-pane fade">
												<h3><?php the_title(); ?> </h3>
												<div class="opis">
													<?php the_field('krotki_opis'); ?>
												</div>
												<a rel="dofollow" href="<?php echo get_permalink( $post->ID ); ?>">Czytaj więcej</a>
											</div>
											<?php
											$menucontent = $menucontent + 1;
										endwhile;
										wp_reset_postdata();
										?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php
				else :
					esc_html_e( 'Ta kategoria jest pusta, uzupełnij ją!', 'text-domain' );
				endif;
				?>
			</div>
		</section>

		<section  id="portfolio" class="panel" data-section-name="portfolio">
			<div class="content-tag">
				<div class="inner-content">
					<h3 class="title">Portfolio</h3>
					<article class="row">
						<div class="col-xl-12">
							<ul class="filtermenu nav wow fadeInRight">
								<?php
								$terms = get_terms('rodzaj');
								$count = count($terms);
								echo '<li data-filter="all">Wszystkie</li>';
								if ( $count > 0 ){
									foreach ( $terms as $term ) {
										$termname = strtolower($term->name);
										$termname = str_replace(' ', '-', $termname);
										echo '<li class="'.$termname.'" data-filter="'.$termname.'">'.$term->name.'</li>';
									}
								}
								?>
							</ul>  
							<div class="tab-content">
								<div class="contain row">
									<?php
									$testimonials = new WP_Query( $args );
									if( $testimonials->have_posts() ) :
										$args = array( 'post_type' => 'realizacja', 'cat' => 34, 'posts_per_page' => -1 );
										$loop = new WP_Query( $args );
										$time = 500;
										$licznikrealizacji = 0;
										while ( $loop->have_posts() ) : $loop->the_post();
											$terms = get_the_terms( $post->ID, 'rodzaj' );
											if ( $terms && ! is_wp_error( $terms ) ) :
												$links = array();
												foreach ( $terms as $term ) {
													$links[] = $term->name;
												}
												$tax_links = join( " ", str_replace(' ', '-', $links));
												$tax = strtolower($tax_links);
											endif;

											echo '<div class="col-md-4 col-sm-6 col-xs-12 post '. $tax .'">';
											echo '<a aria-label="link do realizacji" rel="dofollow" href="'. get_permalink() .'">';
											echo '<div class="box wow zoomIn" data-wow-delay="'. $time .'ms">'; ?>
											<img class="ratio__content lazyload" alt="realizacja" data-src=" <?php the_post_thumbnail_url( 'big' ); ?> 

											">
											<?php
											echo '<div class="box-info"><p>';
											the_title();
											echo '</p></div>';
											echo '</div>';
											echo '</a>';
											echo '</div>';
											$time = $time + 250;
											$licznikrealizacji = $licznikrealizacji + 1;
										endwhile; ?>
										<?php
									else :
										esc_html_e( 'Ta kategoria jest pusta, uzupełnij ją!', 'text-domain' );
									endif;
									?>
									<div class="button-position">
										<a href="/portfolio">sprawdź całe portfolio</a>
									</div>
								</div>
							</div>
						</div>
					</article>
				</div>
				<div class="fly-letter">
					<span>portfolio</span>
				</div>
			</section>
			<?php if( have_rows('kontakt', 'option') ): while( have_rows('kontakt', 'option') ): the_row(); ?>
				<section id="contact" class="panel" data-section-name="contact">
					<div class="content-tag">
						<div class="inner-content">
							<div class="row">
								<div class="col-xl-6">
									<div class="contact-content wow fadeInLeft">
										<h3 class="title">Kontakt</h3>
										<ul>
											<li>
												<div class="icon"><i class="far fa-envelope"></i></div>
												<div class="content">
													<strong>e-mail</strong>
													<a href="mailto:<?php the_sub_field('mail'); ?>"><?php the_sub_field('mail'); ?></a>
												</div>
											</li>
											<li>
												<div class="icon"><i class="fas fa-phone"></i></div>
												<div class="content">
													<strong>telefon</strong>
													<a href="tel:<?php the_sub_field('numer_telefonu'); ?>">
														<?php the_sub_field('numer_telefonu'); ?>
													</a>
												</div>
											</li>
											<li>
												<div class="icon"><i class="fas fa-map-marker"></i></div>
												<div class="content">
													<?php the_sub_field('infomracje_dodatkowe'); ?>
												</div>
											</li>
										</ul>
									</div>
									<div class="credits">Copyright @ 2018 - 2019 by Kordit.pl</div>
								</div>
								<div class="col-xl-6">
									<div class="row form-contact wow fadeInRight">
										<div class="form-home">
											<?php echo do_shortcode('[contact-form-7 id="5" title="Formularz 1"]'); ?>
										</div>

									</div>
								</div>
							</div> 

						</div>

					</section> 
				<?php endwhile; endif; ?>
			</main>
			<?php get_footer(); ?>}