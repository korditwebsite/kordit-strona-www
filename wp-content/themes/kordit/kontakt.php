<?php
/**
* Template Name: Kontakt
*/
?>
<?php get_header(); ?>
<main id="kontakt" data-spy="scroll" data-target="#list-spy" data-offset="0">
	<canvas id="linie" width="1550" height="400"></canvas>
	<?php if( have_rows('kontakt', 'option') ): while( have_rows('kontakt', 'option') ): the_row(); ?>
		<section id="contact" class="panel" data-section-name="contact">
			<content>
				<div class="inner-content">
					<div class="row">
						<div class="col-xl-6">
							<div class="contact-content wow fadeInLeft">
								<h3 class="title">Kontakt</h3>
								<ul>
									<li>
										<a href="#">
											<div class="icon"><i class="far fa-envelope"></i></div>
											<div class="content">
												<strong>e-mail</strong>
												<a href="mailto:<?php the_sub_field('mail'); ?>"><?php the_sub_field('mail'); ?></a>
											</div>
										</a>
									</li>
									<li>
										<a href="#">
											<div class="icon"><i class="fas fa-phone"></i></div>
											<div class="content">
												<strong>telefon</strong>
												<a href="tel:<?php the_sub_field('numer_telefonu'); ?>">
													<?php the_sub_field('numer_telefonu'); ?>
												</a>
											</div>
										</a>
									</li>
									<li>
										<div class="icon"><i class="fas fa-map-marker"></i></div>
										<div class="content">
											<p>
												<?php the_sub_field('infomracje_dodatkowe'); ?>
											</p>
										</div>
									</li>
								</ul>
							</div>
							<div class="credits">Copyright @ 2018 - 2019 by Kordit.pl</div>
						</div>
						<div class="col-xl-6">
							<div class="row form-contact wow fadeInRight">
								<div class="form-home">
									<?php echo do_shortcode('[contact-form-7 id="5" title="Formularz 1"]'); ?>
								</div>

							</div>
						</div>
					</div>

				</content>
			</section>
		<?php endwhile; endif; ?>
	</main>
	<?php get_footer(); ?>