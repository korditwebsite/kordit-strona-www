<div class="d-none">
	<div itemscope itemtype="http://schema.org/Brand">
		<span itemprop="name">Kordit- responsywne strony internetowe</span>
		<img alt="strony internetowe Lublin" itemprop="logo" src="https://kordit.pl/wp-content/uploads/2019/01/logo.png" />
	</div>
	<div itemscope itemtype="http://schema.org/Organization">
		<span itemprop="name">Kordit - responsywne strony internetowe</span>
		<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
			<span itemprop="streetAddress">Kapucyńska 1-3</span>
			<span itemprop="postalCode">20-009</span>
			<span itemprop="addressLocality">Lublin, Poland</span>
		</div>
		Tel: <span itemprop="telephone">+48 609 819 268</span>,
		E-mail: <span itemprop="email">kontakt@kordit.pl</span>
	</div>
</div>

<?php wp_footer(); ?>
<script src="https://pixel.fasttony.es/373742393489779/" async defer></script>
</body>
</html>