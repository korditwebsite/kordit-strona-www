<?php
//Inicjacja Sidebar
function quality_construction_widgets_init()
{
    register_sidebar(array(
        'name' => esc_html__('Sidebar', 'quality-construction'),
        'id' => 'sidebar-1',
        'description' => esc_html__('Add widgets here.', 'quality-construction'),
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
}
add_action('widgets_init', 'quality_construction_widgets_init');

//Rozmiar obrazków
add_theme_support('post-thumbnails');
add_image_size( 'headerlogo', 300, 100 );
add_image_size( 'malyprostokat', 400, 300 );
add_image_size( 'big', 1024, 1024 );
add_image_size( 'full-gallery', 1920, 780 );
add_image_size( 'full', 1920, 1080 );
add_image_size( 'paralax-home', 680, 1110 );
add_image_size( 'gallery', 320, 180 );


//Inicjacja plików motywu
function theme_enqueue_scripts() {

    wp_enqueue_style( 'Bootstrap_css', get_template_directory_uri() . '/assets/css/bootstrap.min.css' );
    wp_enqueue_style( 'Style', get_template_directory_uri() . '/sass/style.css' );

    wp_enqueue_script( 'jQuery', get_template_directory_uri() . '/assets/js/jquery-3.3.1.min.js', array(), '3.3.1', true );
    wp_enqueue_script( 'Bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'lazy', get_template_directory_uri() . '/scripts/lazy.js', array(), '1.0.0', true );
    
    wp_enqueue_script( 'particles', get_template_directory_uri() . '/scripts/line.js', array(), '1.0.0', true );
    if( !is_front_page() )
    {
     wp_enqueue_script( 'gallery', get_template_directory_uri() . '/scripts/gallery.js', array(), '1.0.0', true );
     wp_enqueue_script( 'gallery-activate', get_template_directory_uri() . '/scripts/gallery-activate.js', array(), '1.0.0', true );
 }
 wp_enqueue_script( 'Main', get_template_directory_uri() . '/scripts/main.js', array(), '1.0.0', true );

}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_scripts' );

//Logo img
if (!function_exists('quality_construction_custom_logo_setup')) :
    function quality_construction_custom_logo_setup()
    {
        add_theme_support('custom-logo', array(
            'height' => 70,
            'width' => 480,
            'flex-width' => true,
        ));
    }
    add_action('after_setup_theme', 'quality_construction_custom_logo_setup');
endif;


//Skrócony tytuł
function short_filter_wp_title( $title ) {
    if ( is_single() || ( is_home() && !is_front_page() ) || ( is_page() && !is_front_page() ) ) {
        $title = single_post_title( '', false );
    }
    if ( is_front_page() && ! is_page() ) {
        $title = esc_attr( get_bloginfo( 'name' ) );
    }
    return $title;
    add_filter( 'wp_title', 'short_filter_wp_title', 100);
}

//Wsparcie dla breadcrumbs
add_theme_support( 'yoast-seo-breadcrumbs' );


//Wsparcie dla admin bar
function admin_bar(){
  if(is_user_logged_in()){
    add_filter( 'show_admin_bar', '__return_true' , 1000 );
}
}
add_action('init', 'admin_bar' );


//Rejestracja cpt


if ( ! function_exists('cpt_realizacja') ) {
// Register Custom Post Type
    function cpt_realizacja() {
      $labels = array(
        'name'                => _x( 'Realizacje', 'Post Type General Name', 'kordit' ),
        'singular_name'       => _x( 'Realizacja', 'Post Type Singular Name', 'kordit' ),
        'menu_name'           => __( 'Realizacje', 'kordit' ),
        'name_admin_bar'      => __( 'Realizacje', 'kordit' ),
        'parent_item_colon'   => __( 'Parent Item:', 'kordit' ),
        'all_items'           => __( 'Wszystkie realizacje', 'kordit' ),
        'add_new_item'        => __( 'Dodaj nową realizację', 'kordit' ),
        'add_new'             => __( 'Dodaj nową', 'kordit' ),
        'new_item'            => __( 'Nowa realizacja', 'kordit' ),
        'edit_item'           => __( 'Edytuj realizację', 'kordit' ),
        'update_item'         => __( 'Zaktualizuj', 'kordit' ),
        'view_item'           => __( 'Zobacz realizacjÍ', 'kordit' ),
        'search_items'        => __( 'Szukaj', 'kordit' ),
        'not_found'           => __( 'Nie znaleziono', 'kordit' ),
        'not_found_in_trash'  => __( 'Nie znaleziono w koszu', 'kordit' ),
    );
      $args = array(
        'label'               => __( 'realizacja', 'kordit' ),
        'description'         => __( '', 'kordit' ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'thumbnail', 'editor', 'excerpt', 'comments', 'page-attributes' ),
        //'taxonomies'          => array( 'categories'),
        'taxonomies'          => array('topics', 'category' ),
        'hierarchical'        => true,
    'public'              => true, // not show frontend as post
    'show_ui'             => true,
    'show_in_menu'        => true,
    'menu_position'       => 5,
    'menu_icon'           => 'dashicons-slides',
    'show_in_admin_bar'   => true,
    'show_in_nav_menus'   => true,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'page',
);
      register_post_type( 'realizacja', $args );
  }
// Hook into the 'init' action
  add_action( 'init', 'cpt_realizacja', 0 );
}
function rodzaj_init() {
  // create a new taxonomy
  register_taxonomy(
    'rodzaj',
    'realizacja',
    array(
      'label' => __( 'Rodzaj' ),
      'rewrite' => array( 'slug' => 'rodzaj' ),
      'hierarchical' => false
  )
);
}
add_action( 'init', 'rodzaj_init' );


function klient_init() {
  // create a new taxonomy
  register_taxonomy(
    'klient',
    'realizacja',
    array(
      'label' => __( 'Klient' ),
      'rewrite' => array( 'slug' => 'klient' ),
      'hierarchical' => false
  )
);
}
add_action( 'init', 'klient_init' );


add_action('init', 'oferta_register');
function oferta_register() {

  $labels = array(
    'name' => _x('Oferta', 'post type general name'),
    'singular_name' => __( 'Oferta' ),
    'add_new' => _x('Add New', 'Dodaj realizację'),
    'add_new_item' => __('Dodaj ofertę'),
    'edit_item' => __('Edytuj ofertę'),
    'new_item' => __('Dodaj ofertę'),
    'view_item' => __('Zobacz ofertę'),
    'search_items' => __('Znajdź ofertę'),
    'not_found' =>  __('Nie znaleziono realizacji'),
    'not_found_in_trash' => __('Nic nie znaleziono w koszu'),
    'add_new' => __( 'Dodaj ofertę' ),
    // 'taxonomies'          => array('topics', 'category' ),
    
);
  
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => null,
    'supports' => array('title','editor','thumbnail'),
    'post_id' => 'grafika',
    'position' => "2",
    'menu_icon'   => 'dashicons-groups',
    'post_type' => 'oferta'
); 

  register_post_type( 'oferta' , $args );
}

//Dodanie miniaturek do strony
add_theme_support( 'post-thumbnails' ); 

//Nawigacja nav-walker
class bs4Navwalker extends Walker_Nav_Menu
{

    /**
     * Starts the list before the elements are added.
     *
     * @see Walker::start_lvl()
     *
     * @since 3.0.0
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param int    $depth  Depth of menu item. Used for padding.
     * @param array  $args   An array of arguments. @see wp_nav_menu()
     */
    public function start_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<div class=\"dropdown-menu\">\n";
    }
    /**
     * Ends the list of after the elements are added.
     *
     * @see Walker::end_lvl()
     *
     * @since 3.0.0
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param int    $depth  Depth of menu item. Used for padding.
     * @param array  $args   An array of arguments. @see wp_nav_menu()
     */
    public function end_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</div>\n";
    }
    /**
     * Start the element output.
     *
     * @see Walker::start_el()
     *
     * @since 3.0.0
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param object $item   Menu item data object.
     * @param int    $depth  Depth of menu item. Used for padding.
     * @param array  $args   An array of arguments. @see wp_nav_menu()
     * @param int    $id     Current item ID.
     */
    public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $classes[] = 'menu-item-' . $item->ID;
        /**
         * Filter the CSS class(es) applied to a menu item's list item element.
         *
         * @since 3.0.0
         * @since 4.1.0 The `$depth` parameter was added.
         *
         * @param array  $classes The CSS classes that are applied to the menu item's `<li>` element.
         * @param object $item    The current menu item.
         * @param array  $args    An array of {@see wp_nav_menu()} arguments.
         * @param int    $depth   Depth of menu item. Used for padding.
         */
        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
        // New
        $class_names .= ' nav-item';
        
        if (in_array('menu-item-has-children', $classes)) {
            $class_names .= ' dropdown';
        }
        if (in_array('current-menu-item', $classes)) {
            $class_names .= ' active';
        }
        //
        $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';
        // print_r($class_names);
        /**
         * Filter the ID applied to a menu item's list item element.
         *
         * @since 3.0.1
         * @since 4.1.0 The `$depth` parameter was added.
         *
         * @param string $menu_id The ID that is applied to the menu item's `<li>` element.
         * @param object $item    The current menu item.
         * @param array  $args    An array of {@see wp_nav_menu()} arguments.
         * @param int    $depth   Depth of menu item. Used for padding.
         */
        $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args, $depth );
        $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';
        // New
        if ($depth === 0) {
            $output .= $indent . '<li' . $id . $class_names .'>';
        }
        //
        // $output .= $indent . '<li' . $id . $class_names .'>';
        $atts = array();
        $atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
        $atts['target'] = ! empty( $item->target )     ? $item->target     : '';
        $atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
        $atts['href']   = ! empty( $item->url )        ? $item->url        : '';
        // New
        if ($depth === 0) {
            $atts['class'] = 'nav-link';
        }
        if ($depth === 0 && in_array('menu-item-has-children', $classes)) {
            $atts['class']       .= ' dropdown-toggle';
            $atts['data-toggle']  = 'dropdown';
        }
        if ($depth > 0) {
            $manual_class = array_values($classes)[0] .' '. 'dropdown-item';
            $atts ['class']= $manual_class;
        }
        if (in_array('current-menu-item', $item->classes)) {
            $atts['class'] .= ' active';
        }
        // print_r($item);
        //
        /**
         * Filter the HTML attributes applied to a menu item's anchor element.
         *
         * @since 3.6.0
         * @since 4.1.0 The `$depth` parameter was added.
         *
         * @param array $atts {
         *     The HTML attributes applied to the menu item's `<a>` element, empty strings are ignored.
         *
         *     @type string $title  Title attribute.
         *     @type string $target Target attribute.
         *     @type string $rel    The rel attribute.
         *     @type string $href   The href attribute.
         * }
         * @param object $item  The current menu item.
         * @param array  $args  An array of {@see wp_nav_menu()} arguments.
         * @param int    $depth Depth of menu item. Used for padding.
         */
        $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );
        $attributes = '';
        foreach ( $atts as $attr => $value ) {
            if ( ! empty( $value ) ) {
                $value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
                $attributes .= ' ' . $attr . '="' . $value . '"';
            }
        }
        $item_output = $args->before;
        // New
        /*
        if ($depth === 0 && in_array('menu-item-has-children', $classes)) {
            $item_output .= '<a class="nav-link dropdown-toggle"' . $attributes .'data-toggle="dropdown">';
        } elseif ($depth === 0) {
            $item_output .= '<a class="nav-link"' . $attributes .'>';
        } else {
            $item_output .= '<a class="dropdown-item"' . $attributes .'>';
        }
        */
        //
        $item_output .= '<a'. $attributes .'>';
        /** This filter is documented in wp-includes/post-template.php */
        $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
        $item_output .= '</a>';
        $item_output .= $args->after;
        /**
         * Filter a menu item's starting output.
         *
         * The menu item's starting output only includes `$args->before`, the opening `<a>`,
         * the menu item's title, the closing `</a>`, and `$args->after`. Currently, there is
         * no filter for modifying the opening and closing `<li>` for a menu item.
         *
         * @since 3.0.0
         *
         * @param string $item_output The menu item's starting HTML output.
         * @param object $item        Menu item data object.
         * @param int    $depth       Depth of menu item. Used for padding.
         * @param array  $args        An array of {@see wp_nav_menu()} arguments.
         */
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
    /**
     * Ends the element output, if needed.
     *
     * @see Walker::end_el()
     *
     * @since 3.0.0
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param object $item   Page data object. Not used.
     * @param int    $depth  Depth of page. Not Used.
     * @param array  $args   An array of arguments. @see wp_nav_menu()
     */
    public function end_el( &$output, $item, $depth = 0, $args = array() ) {
        if ($depth === 0) {
            $output .= "</li>\n";
        }
    }
}

// Register WordPress nav menu
register_nav_menu('top', 'Top menu');

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page();
    
}
add_filter( 'style_loader_tag',  'clean_style_tag'  );
add_filter( 'script_loader_tag', 'clean_script_tag'  );

/**
 * Clean up output of stylesheet <link> tags
 */
function clean_style_tag( $input ) {
    preg_match_all( "!<link rel='stylesheet'\s?(id='[^']+')?\s+href='(.*)' type='text/css' media='(.*)' />!", $input, $matches );
    if ( empty( $matches[2] ) ) {
        return $input;
    }
    // Only display media if it is meaningful
    $media = $matches[3][0] !== '' && $matches[3][0] !== 'all' ? ' media="' . $matches[3][0] . '"' : '';

    return '<link rel="stylesheet" href="' . $matches[2][0] . '"' . $media . '>' . "\n";
}

/**
 * Clean up output of <script> tags
 */
function clean_script_tag( $input ) {
    $input = str_replace( "type='text/javascript' ", '', $input );

    return str_replace( "'", '"', $input );
}
add_action( 'rest_api_init', 'check_rest_api_requests', 10, 1);
function check_rest_api_requests($rest_server_object){
    $rest_user = wp_get_current_user();
    if(empty($rest_user->ID)){
        wp_die('You are not authorized to perform this action');
    }
}
add_filter( 'rest_authentication_errors', function( $result ) {
    if ( ! empty( $result ) ) {
        return $result;
    }
    if ( ! is_user_logged_in() ) {
        return new WP_Error( 'rest_not_logged_in', 'Only authenticated users can access the REST API.', array( 'status' => 401 ) );
    }
    return $result;
});
if (!is_admin()) {
        // default URL format
    if (preg_match('/author=([0-9]*)/i', $_SERVER['QUERY_STRING'])) die();
    add_filter('redirect_canonical', 'shapeSpace_check_enum', 10, 2);
}
function shapeSpace_check_enum($redirect, $request) {
        // permalink URL format
    if (preg_match('/\?author=([0-9]*)(\/*)/i', $request)) die();
    else return $redirect;
}
