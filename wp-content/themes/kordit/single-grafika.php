<?php
/*
Template Name: Grafika
Template Post Type: oferta
*/
get_header(); ?>
<main id="oferta">
	<canvas id="linie" width="1550" height="400"></canvas>
	<div class="title-content">
		<div class="container">
			<h1><?php the_title(); ?></h1>
			<p id="breadcrumbs">
				<a href="/">Home</a> » 
				<a href="/oferta">Oferta</a> »
				<span class="breadcrumb_last"><?php the_title(); ?></span>
			</p>
		</div>
	</div>
	<article>
		<div id="dla_kogo" class="container-fluid">
			<div class="row">
				<div class="col-xl-6 col-md-6 col-12">
					<div class="tresc">
						<h3 class="subtitle">To idealny wybór, jeśli</h3>
						<?php the_field("dla_kogo"); ?>
					</div>
				</div>
				<div class="col-xl-6 col-md-6 col-12">
					<div class="row">
						<?php the_post_thumbnail( 'big', "", array( "class" => "img-fluid", "alt" => "realizacje" ) ); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="wypunktowania container-fluid">
			<div class="item">
				<div class="row">
					<div class="col-xl-6 col-md-6 col-12">
						<div class="row">
							<div class="thumbnail">
								<img class="img-fluid" src="/wp-content/uploads/2019/01/zalety.jpg">
							</div>
						</div>
					</div>
					<div class="col-xl-6 col-md-6 col-12">
						<div class="inner-content">
							<h3 class="subtitle">Zalety tego rozwiązania:</h3>
							<ul>
								<?php if( have_rows('zalety') ): while ( have_rows('zalety') ) : the_row(); ?>
									<li><?php the_sub_field('item'); ?></li>
								<?php endwhile; else : endif; ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="item">
				<div class="row">
					<div class="col-xl-6 col-md-6 col-12">
						<div class="inner-content">
							<h3 class="subtitle">To rozwiązanie wyróżnia się:</h3>
							<ul>
								<?php if( have_rows('wyroznienie') ): while ( have_rows('wyroznienie') ) : the_row(); ?>
									<li><?php the_sub_field('item'); ?></li>
								<?php endwhile; else : endif; ?>
							</ul>
						</div>
					</div>
					<div class="col-xl-6 col-md-6 col-12">
						<div class="row">
							<div class="thumbnail">
								<img class="img-fluid" src="/wp-content/uploads/2019/01/wyroznienia.jpg">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="icons">
			<div class="row">
				<div class="col-xl-12">
					<h2>Dlaczego warto wybrać nasze usługi?</h2>
				</div>
				<div class="col-xl-4 col-md-4 col-sm-6">
					<div class="row">
						<div class="item">
							<i class="far fa-user-circle"></i>
							<h3>Indywidualne podejście</h3>
							<p>Projekty stworzone przez nas są niepowtarzalne!</p>
						</div>
					</div>
				</div>
				<div class="col-xl-4 col-md-4 col-sm-6">
					<div class="item">
						<div class="row">
							<img src="/wp-content/uploads/2019/01/grap1.jpg">
						</div>
					</div>
				</div>
				<div class="col-xl-4 col-md-4 col-sm-6">
					<div class="row">
						<div class="item">
							<i class="fas fa-trophy"></i>
							<h3>Najwyższa jakość</h3>
							<p style="padding:0px 40px;">Do każdego projektu podchodzimy z należytą starannością, dlatego nasze projekty są na najwyższym poziomie!</p>
						</div>
					</div> 
				</div>
				<div class="col-xl-4 col-md-4 col-sm-6">
					<div class="item">
						<div class="row">
							<img src="/wp-content/uploads/2019/01/grap2.jpg">
						</div>
					</div>
				</div>
				<div class="col-xl-4 col-md-4 col-sm-6">
					<div class="row">
						<div class="item">
							<i class="far fa-gem"></i>
							<h3>Kreatywny design</h3>
							<p>Nasi graficy zadowolą nawet nabardziej wymagajacych klientów!</p>
						</div>
					</div>
				</div>
				<div class="col-xl-4 col-md-4 col-sm-6">
					<div class="item">
						<div class="row">
							<img src="/wp-content/uploads/2019/01/grap3.jpg">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container looks">
	<div class="row">
		<?php $number = get_field("numer"); ?>
		<h3 class="title text-center mt-5 mb-3">Sprawdź realizacje <?php the_title(); ?></h3>
		<?php
		$nazwaKategori = "realizacje";
		$cat = ( isset( $_GET['wyroznione'] ) ) ? $_GET['wyroznione'] : 1;
		$args = array(
			'post_type'   => 'realizacja',
			'post_status' => 'publish',
			'order' => 'ASC',
			'posts_per_page'=>'3',
			'tax_query' => [
				[

					'taxonomy' => 'rodzaj',
					'field' => 'term_id',
					'terms' => $number,
				]
			],
		);

		$testimonials = new WP_Query( $args ); 
		// echo "<pre>"; print_r($testimonials);
		if( $testimonials->have_posts() ) :
			?>
			<?php
			while( $testimonials->have_posts() ) :
				$testimonials->the_post();
				?>
				<div class="col-lg-4 col-12">
					<div class="item-portfolio">
						<?php
						if ( has_post_thumbnail()  ) {
							the_post_thumbnail( 'home-thumbnail' );
						}
						?>
						<a href="<?php the_permalink(); ?>"><button>Sprawdź realizacje</button></a>
						<h5><?php printf(get_the_title());  ?></h5>
					</div>
				</div>
				<?php
			endwhile;
			wp_reset_postdata();
			?>
			<?php
		else :
			esc_html_e( 'Kategoria w trakcie uzupełniania, zapraszamy wkrótce!', 'text-domain' );
		endif;
		?>
	</div>
</div>
</article>
<div id="cta">
	<div class="container">
		<div class="row">
			<div class="col-xl-8 offset-xl-2">
				<h3 class="title">Zainteresowany ofertą?</h3>
				<?php if( have_rows('kontakt', 'option') ): while( have_rows('kontakt', 'option') ): the_row(); ?>
					<div class="buttons mb-5 pb-5">
						<a href="tel:<?php the_sub_field('numer_telefonu'); ?>">
							<button>Zadzwoń <?php the_sub_field('numer_telefonu'); ?></button>
						</a>
						<?php $atribute = "usługa " . short_filter_wp_title( $title ); ?>
						<a data-toggle="modal" data-atrr="<?php echo $atribute; ?>" data-target="#popup" href="#" class="button-price"><button>zamów usługę</button></a>
					</div>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</div>
</div>
<?php get_template_part( 'popup'); ?>
</main>
<?php get_footer(); ?>
<script type="text/javascript">
	$("#value").val("Jestem zainteresowany/a pozycją <?php the_title(); ?>");
	jQuery(document).ready(function($){
		$(document).ready(function() {
			$(".button-price").on("click", function(){
				$("#value").val($(this).data('atrr'));
			})
		})    

		$('#value"').attr('readonly', true);
		$("#name").unwrap();   
	});
</script>
sc