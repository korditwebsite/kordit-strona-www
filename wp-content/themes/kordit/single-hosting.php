<?php
/*
Template Name: Hosting/domena
Template Post Type: oferta
*/
get_header(); ?>
<main id="oferta">
	<canvas id="linie" width="1550" height="400"></canvas>
	<div class="title-content">
		<div class="container">
			<h1><?php the_title(); ?></h1>
			<?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
		</div>
	</div>
	<article class="mb-5">
		<div id="dla_kogo" class="container-fluid">
			<div class="row">
				<div class="col-xl-6">
					<div class="tresc">
						<h3 class="subtitle">To idealny wybór, jeśli</h3>
						<?php the_field("dla_kogo"); ?>
					</div>
				</div>
				<div class="col-xl-6">
					<div class="row">
						<?php the_post_thumbnail( 'big', "", array( "class" => "img-fluid", "alt" => "realizacje" ) ); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="wypunktowania container-fluid">
			<div class="item">
				<div class="row">
					<div class="col-xl-6">
						<div class="row">
							<div class="thumbnail">
								<img class="img-fluid" src="/wp-content/uploads/2019/01/zalety.jpg">
							</div>
						</div>
					</div>
					<div class="col-xl-6">
						<div class="inner-content">
							<h3 class="subtitle">Zalety tego rozwiązania:</h3>
							<ul>
								<?php if( have_rows('zalety') ): while ( have_rows('zalety') ) : the_row(); ?>
									<li><?php the_sub_field('item'); ?></li>
								<?php endwhile; else : endif; ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="item">
				<div class="row">
					<div class="col-xl-6">
						<div class="inner-content">
							<h3 class="subtitle">To rozwiązanie wyróżnia się:</h3>
							<ul>
								<?php if( have_rows('wyroznienie') ): while ( have_rows('wyroznienie') ) : the_row(); ?>
									<li><?php the_sub_field('item'); ?></li>
								<?php endwhile; else : endif; ?>
							</ul>
						</div>
					</div>
					<div class="col-xl-6">
						<div class="row">
							<div class="thumbnail">
								<img class="img-fluid" src="/wp-content/uploads/2019/01/wyroznienia.jpg">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="icons">
			<div class="row">
				<div class="col-xl-12">
					<h2>Dlaczego warto wybrać nasze usługi?</h2>
				</div>
				<div class="col-xl-4">
					<div class="row">
						<div class="item">
							<i class="fas fa-mobile"></i>
							<h3>Responsywność</h3>
							<p>Nasze strony internetowe są dostosowane do urządzeń mobilnych!</p>
						</div>
					</div>
				</div>
				<div class="col-xl-4">
					<div class="item">
						<div class="row">
							<img src="https://kordit.pl/wp-content/uploads/2019/01/1.jpg">
						</div>
					</div>
				</div>
				<div class="col-xl-4">
					<div class="row">
						<div class="item">
							<i class="fab fa-google"></i>
							<h3>Optymalizacja SEO</h3>
							<p>W standardzie dostosowujemy stronę do wyszukiwarki Google!</p>
						</div>
					</div>
				</div>
				<div class="col-xl-4">
					<div class="item">
						<div class="row">
							<img src="https://kordit.pl/wp-content/uploads/2019/01/4.jpg">
						</div>
					</div>
				</div>
				<div class="col-xl-4">
					<div class="row">
						<div class="item">
							<i class="far fa-gem"></i>
							<h3>Kreatywny design</h3>
							<p>Nasi graficy zadowolą nawet nabardziej wymagajacych klientów!</p>
						</div>
					</div>
				</div>
				<div class="col-xl-4">
					<div class="item">
						<div class="row">
							<img src="https://kordit.pl/wp-content/uploads/2019/01/3.jpg">
						</div>
					</div>
				</div>
				<div class="col-xl-4">
					<div class="row">
						<div class="item">
							<i class="fas fa-columns"></i>
							<h3>Panel administracyjny</h3>
							<p><b>Wszystkie</b> strony posiadają intuicyjny panel administracyjny!</p>
						</div>
					</div>
				</div>
				<div class="col-xl-4">
					<div class="item">
						<div class="row">
							<img src="https://kordit.pl/wp-content/uploads/2019/01/2.jpg">
						</div>
					</div>
				</div>
				<div class="col-xl-4">
					<div class="row">
						<div class="item">
							<i class="fas fa-bolt"></i>
							<h3>Szybkość wykonania</h3>
							<p>Projekt realizujemy nawet w 72h!</p>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
</article>
<div id="cta">
	<div class="container">
		<div class="row">
			<div class="col-xl-8 offset-xl-2">
				<h3 class="title">Zainteresowany ofertą?</h3>
				<?php if( have_rows('kontakt', 'option') ): while( have_rows('kontakt', 'option') ): the_row(); ?>
					<div class="buttons my-5 pb-5">
						<a href="tel:<?php the_sub_field('numer_telefonu'); ?>">
							<button>Zadzwoń <?php the_sub_field('numer_telefonu'); ?></button>
						</a>
						<?php $atribute = short_filter_wp_title( $title ); ?>
						<a data-toggle="modal" data-atrr="<?php echo $atribute; ?>" data-target="#popup" href="#" class="button-price"><button>zamów usługę</button></a>
					</div>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</div>
</div>
<?php get_template_part( 'popup'); ?>
</main>
<?php get_footer(); ?>
<script type="text/javascript">
	$("#value").val("Jestem zainteresowany/a pozycją <?php the_title(); ?>");
	jQuery(document).ready(function($){
		$(document).ready(function() {
			$(".button-price").on("click", function(){
				$("#value").val($(this).data('atrr'));
			})
		})    

		$('#value"').attr('readonly', true);
		$("#name").unwrap();   
	});
</script>