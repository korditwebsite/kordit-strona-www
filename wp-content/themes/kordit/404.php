<?php get_header(); ?>
<canvas id="linie" width="1550" height="400"></canvas>
<main class="p-relative" style="display: flex;justify-content: center;align-items:center; height:100vh;flex-direction: column; z-index: 100; position:relative;" id="kontakt">
	<h1>Nie znaleziono strony</h1>
	<a href="/">
		<button style="margin-bottom: 15px; text-transform: uppercase; color: #2e332f; font-size: 14px; letter-spacing: 3px; font-weight: 700; cursor: pointer; border: solid 1px #2e332f; padding: 5px;">Wróć do głównej strony</button>
	</a>
</main>
<?php get_footer(); ?>
