<?php
$args = array(
	'post_type'   => "realizacja",
	'post_status' => 'publish',
	'posts_per_page' => '99',
	'cat'            => 34,
	'order' => 'ASC'
);
$testimonials = new WP_Query( $args );
if( $testimonials->have_posts() ) :
	$args = array( 'post_type' => 'realizacja', 'posts_per_page' => -1 );
	$loop = new WP_Query( $args );
	while ( $loop->have_posts() ) : $loop->the_post();
		$terms = get_the_terms( $post->ID, 'rodzaj' );
		if ( $terms && ! is_wp_error( $terms ) ) :
			$links = array();
			foreach ( $terms as $term ) {
				$links[] = $term->name;
			}
			$tax_links = join( " ", str_replace(' ', '-', $links));
			$tax = strtolower($tax_links);
		endif;

		echo '<div class="col-md-4 col-sm-6 col-xs-12 post '. $tax .'">';
		echo '<a href="'. get_permalink() .'">';
		echo '<div class="box" style="background-image: url('. wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ) .');">';
		echo '<div class="box-info"><p>';
		the_title();
		echo '</p></div>';
		echo '</div>';
		echo '</a>';
		echo '</div>';
	endwhile; ?>
	<?php
else :
	esc_html_e( 'Ta kategoria jest pusta, uzupełnij ją!', 'text-domain' );
endif;
?>